# steady-control

## Краткое описание
```
 Построения на бэкенде структуры данных в виде дерева и ее обход с выводом на фронте
 полное описание задания в файле "задание.pdf"
```
## Сервер запускать по пути
```
\src\api\app.js   node app.js
```
## Файлы с данными ответа сервера
```
\src\api\files\citizens.json
\src\api\files\cities.json
```
## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
