const db = require('../dbInstance.js');
const mondoose = require('mongoose');

let schema = new mondoose.Schema({
    id: Number,
    name: String,
    data: String,
});

module.exports = db.model('Cities', schema, 'cities');