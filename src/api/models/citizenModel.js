const db = require('../dbInstance.js');
const mondoose = require('mongoose');

let schema = new mondoose.Schema({
    id: Number,
    name: String,
    city_id: Number,
    groups: Array,
});

module.exports = db.model('Citizen', schema, 'citizens');