const app = require('express')();
const citizenRouter = require('./routes/citizensRouter.js');
const cors = require('cors');
app.use(cors());

app.use('/api/citizen/', citizenRouter);

app.listen('3001', ()=>{
    console.log('server is run port:3001');
})