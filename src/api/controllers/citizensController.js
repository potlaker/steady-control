let CitizenModel = require('../models/citizenModel.js');
let Cities = require('../models/cityModel.js');

/**
 * Строит дерево адресов жителей, получает численность городов и отправляет на фронт
 * Все на основе данных из data base
 * @param {object} request - объект запроса
 * @param {object} responce - объект ответа
 */
exports.getTree = async function(request, responce){
    try{
        let citizens =  await CitizenModel.find({});
        let result = {uncertain: []};
        let listItem = [
            'country',
            'city',
            'district',
            'street',
            'home',
            'resident',
        ];
        for(let resident of citizens){
            if('groups' in resident){
                setPlace(resident, result, listItem[Symbol.iterator]());
            }else{
                result.uncertain.push(resident.name);
            }
        }
        let citiesPopulation = await getCities();

        responce.status(200).send({
            status: true,
            data: {citizens: result, cities: citiesPopulation},
        });
    }catch(err){
        console.log(err);
        responce.status(200).send({
            status: false,
            title: 'Не удалось обработать запрос',
            name: err.name,
            message: err.message,
        });
    }
}
/**
 * Строит дерево адресов жителей
 * @param {array} resident - массив объектов Document
 * @param {object} result - ассоциативный массив результатов функции
 * @param {object} iterator - итератор массива полей
 * @returns {undefined} - ничего не возвращает, редактирует массив result
 */
function setPlace(resident, result, iterator){
    let resIter = iterator.next();
    if(resIter.done){ return }
    if(resIter.value == 'resident'){
        setResident(resident, result);
        return;
    }
    let countryObj = resident.groups.toObject().find( data => data.type == resIter.value);
    if(countryObj){
        if(!result[countryObj.name]){
            result[countryObj.name] = {name: countryObj.name, type: resIter.value, childs: {}};
        }
        setPlace(resident, result[countryObj.name].childs, iterator);
    }else{
        setPlace(resident, result, iterator);
    }
}
/**
 * Добавляет жителей в общий массив citizens
 * @param {array} resident - массив объектов Document
 * @param {object} result - ассоциативный массив результатов функции
 * @returns {undefined} - ничего не возвращает, редактирует массив result
 */
function setResident(resident, result){
    if(resident.name){
        if(result.citizens){
            result.citizens.push(resident.name);
        }else{
            result.citizens = [resident.name];
        }
        result.cityId = resident.id;
    }
}
/**
 * Возвращает ассоциативный массив {город: население}
 * @returns {object} - массив {город: население}
 */
async function getCities(){
    let cities =  await Cities.find({});
    let result = [];
    for(city of cities){
        result.push({name:city.name, id: city.id, data: city.data});
    }
    return result;
}