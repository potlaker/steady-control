const express = require('express');
const citizenRouter = express.Router();
const controller = require('../controllers/citizensController.js');

citizenRouter.get('/tree', controller.getTree);

module.exports = citizenRouter;