import { createStore } from 'vuex'
import tree from './tree.js';

export default createStore({
  modules: {
    tree,
  }
})
