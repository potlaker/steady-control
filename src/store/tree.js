export default {
    namespaced: true,
    state: {
      citizens: null,
      cities: null,
    },
    getters: {
      getCitizens(state){
        return state.citizens;
      },
      getCities(state){
        return state.cities;
      },
    },
    mutations: {
      setCitizens(state, data){
        console.dir(data);
        state.citizens = data;
      },
      setCities(state, data){
        state.cities = data;
      },
    },
    actions: {
      putCitizens({commit}, data){
        if(!data.uncertain.length){
          delete data.uncertain;
        }
        commit('setCitizens', data);
      },
      putCities({commit}, data){
        commit('setCities', data);
      },
    },
  }